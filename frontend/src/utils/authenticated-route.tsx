
import React, { FC, ReactNode } from 'react';
import { Route } from 'react-router-dom';
import { Alert } from 'react-bootstrap';

type Props = {
  authenticated: boolean;
  path: string;
  children: ReactNode;
}

const AuthenticatedRoute: FC<Props> = (props: Props): JSX.Element => {
  const { authenticated, path, children } = props;
  if (authenticated) {
    return <Route path={path}>{children}</Route>
  } else {
    return <Alert variant={'danger'}>You must be logged in to see this page.</Alert>
  }

}

export default AuthenticatedRoute;