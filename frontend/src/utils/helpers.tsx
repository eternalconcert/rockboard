import { useEffect, useState } from "react";
import { HttpError } from "../exceptions";


export function fetchJson<T> (url: string): Promise<T> {
    return fetch(url, {credentials: 'include'}).then((response) => {
      if (response.status === 200) {
        return response.json()
     } else {
       throw new HttpError(response.status, 'HttpError');
     }
    });
}

export function postJson<T> (url: string, body: {}): Promise<T> {
  return fetch(url, {
    method: 'POST',
    headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(body),
    credentials: 'include'}).then((response) => response.json());
}


export function usePromiseFactory<T>(fetcher: (args?: string) => Promise<T>, args?: string): T | null {
    const [ state, setState ] = useState<T | null>(null);
    useEffect(() => {
      fetcher(args).then(setState);
    }, [args, fetcher]);
    return state;
}

export const titlelize = (word: string): string => {
  return word.charAt(0).toUpperCase() + word.slice(1);
}