import React, { FC } from "react";
import { useParams } from "react-router-dom";
import { Dictionary } from "../types";

const codes: Dictionary<string> = {
  404: '404: Not Found',
  500: '500: Internal Server Error',
}

const ErrorPage: FC = () => {
  const { statusCode } = useParams<{statusCode: string}>();
  return <>{codes[statusCode]}</>
};

export default ErrorPage;
