import React, { ReactNode } from 'react';
import {
  Link
} from 'react-router-dom';
import { titlelize } from '../utils/helpers';

type HeaderPropsType = {
  boardName: string;
  username: string;
};



class Header extends React.Component<HeaderPropsType, {}> {

  render = (): ReactNode => {
    const { boardName } = this.props;
    const { username } = this.props;
    return (
      <header>
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <a className="navbar-brand" href="/">{boardName}</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link" href="/">Home <span className="sr-only">(current)</span></a>
              </li>
            </ul>
            <div>
              {!username &&
                <Link className="login-logout-link" to={'/user/login'}>Login</Link>
              }
              {username &&
                <>
                  <span className={'logged-in-as'}>
                    Logged in as <Link to={`/user/profile/${username}`} >{titlelize(username)}{' '}</Link>
                  </span>
                  <Link className="login-logout-link" to={'/user/logout'}>Logout</Link>
                </>
              }
            </div>
          </div>
        </nav>
      </header>
      )
    }
}


export default Header;