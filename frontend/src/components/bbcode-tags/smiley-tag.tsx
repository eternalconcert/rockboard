import React from 'react';
import { Tag } from 'bbcode-to-react';
import Dictionary from '../../utils/dictionary';


const smileys: Dictionary<string> = {
  'pommes': '🤘',
  'engel': '😇',
  'wink': '😉',
  'eek': '😯',
  'sad': '😟',
  'smile': '😊',
  'grin': '😁',
  '???': '😕',
  'cool': '😎',
  'mad': '😡',
  'clown': '🤡',
  'gaehn': '🥱',
  'nerv': '🙄',
  'heul': '😭',
  'hmpf': '😐',
  'gelb': ':gelb:',
  'rot': ':rot:',
  'gelbrot': ':gelbrot:',
  'ka': '😕',
  'kratz': '🤔',
  'prost': '🍻',
  'sabber': '🤤',
  'klatsch': '👏',
  'traenenlach': '🤣',
  'sick': '🤒',
}


class SmileyTag extends Tag {
  toReact(): JSX.Element {
    const { smiley } = this.params;
  return <span className={`smiley ${smiley}`}>{smileys[smiley]}</span>
  }
}

export default SmileyTag;