import parser from 'bbcode-to-react';
import SmileyTag from './smiley-tag';
import QuoteTag from './quote-tag';
import YoutubeTag from './youtube-tag';


parser.registerTag('quote', QuoteTag)
parser.registerTag('smiley', SmileyTag)
parser.registerTag('smiley', SmileyTag)
parser.registerTag('youtube', YoutubeTag)

export default parser;