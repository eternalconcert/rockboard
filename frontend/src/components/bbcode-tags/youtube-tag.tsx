import React, { FC, useState } from 'react';
import { Tag } from 'bbcode-to-react';


type Props = {
  content: string;
}


const ConfimableYoutubeContent: FC<Props> = (props: Props): JSX.Element => {
  const [confirmed, setConfirmed] = useState(false);

    if(!confirmed) {
      return (
        <span className={'youtube-warning'}
          onClick={(): void => { setConfirmed(true); }}></span>
      )
    }

    const { content } = props;
    return (
      <iframe
        title={'youtube'}
        frameBorder={'0'}
        src={`https://www.youtube.com/embed/${content}`}
        width={560}
        height={315}
        allowFullScreen={true} />
    )
  }



class YoutubeTag extends Tag {
  toReact(): JSX.Element {
    const content: string =  this.getContent(true);
    console.log(content)
    return <ConfimableYoutubeContent content={content} />
  }
}

export default YoutubeTag;