import React from 'react';
import { Tag } from 'bbcode-to-react';


class QuoteTag extends Tag {
  toReact(): JSX.Element {
    const { quote } = this.params;
    return <span className={'quote'}>{quote}: <i>{this.getComponents()}</i></span>
  }
}

export default QuoteTag;