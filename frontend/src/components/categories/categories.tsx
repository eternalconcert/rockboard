import React, { FC } from 'react';
import { Category } from '../../types';
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import SectionHeader from '../section-header';
import { fetchJson, usePromiseFactory } from '../../utils/helpers';
import { Link } from 'react-router-dom';
import { baseApiUrl } from '../../app';


interface CateoriesApi {
  categories: Category[];
}


function fetchCategories(): Promise<Category[]> {
  return fetchJson<CateoriesApi>(`${baseApiUrl}/categories/`)
    .then(data => data.categories)
}


const Categories: FC<{}> = (): JSX.Element => {
  const categories = usePromiseFactory(fetchCategories);

  if (categories) {
    return (
      <Container fluid={'md'}>
        { categories && categories.map(category => {
        const { id, name } = category;
        return (
          <div key={id}>
            <Row>
              <Col><SectionHeader name={name} /></Col>
            </Row>
            <Row>
              { category.forums && category.forums.map(forum => {
                const { id: forumId, name: forumName, description, unread } = forum;
                return (
                  <Col key={forumId} md={4}>
                    <Card>
                      <Card.Body>
                        <Card.Title>
                          <Link className={'card-title'} to={`/forums/${forumId}`}>{forumName}</Link>
                          { unread && <Badge className={'unread-badge'} variant={'dark'}>New</Badge> }
                        </Card.Title>
                        <Card.Text>{description}</Card.Text>
                      </Card.Body>
                    </Card>
                  </Col>
                )
              })}
            </Row>
          </div>
          )
        }) }
      </Container>
    )
  }

  return <div className={'spinner'}></div>;
}

export default Categories;
