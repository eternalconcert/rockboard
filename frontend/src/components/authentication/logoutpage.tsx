import React, { ReactNode } from 'react';
import { logOutUser } from '../../flux/actions';
import { fetchJson } from '../../utils/helpers';
import { Redirect } from 'react-router-dom';
import { baseApiUrl } from '../../app';


function fetchLogout(): Promise<{success: boolean}> {
  return fetchJson<{success: boolean}>(`${baseApiUrl}/logout`)
    .then(data => {
      return (
        {
          success: data.success
        }
      )
    }
  )
}


type LogoutPageState = {
  loggedIn: boolean;
}


class LogoutPage extends React.Component<{}, LogoutPageState> {
  constructor(props: {}) {
    super(props)
    this.state = { loggedIn: true };
  }

  componentDidMount(): void {
    fetchLogout().then(() => {
      this.setState({
        loggedIn: false,
      })
      logOutUser();
    })
  }

  render = (): ReactNode => {
    if (this.state.loggedIn) {
      return <div className={'spinner'}></div>;
    } else {
      return <Redirect to={'/'} />
    }

  }
}

export default LogoutPage;