import React, { FormEvent, ReactNode } from 'react';
import { Alert, Button, Form } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { logInUser } from '../../flux/actions';
import { AuthApi } from '../../types';
import { postJson } from '../../utils/helpers';
import { baseApiUrl } from '../../app';


function postLogin(email: string, password: string): Promise<AuthApi> {
  return postJson<AuthApi>(`${baseApiUrl}/login`, {email, password})
    .then(data => {
      return (
        {
          username: data.username,
          email: data.email,
          roles: data.roles,
          errors: data.errors,
        }
      )
    }
  )
}


type LoginPageState = {
  userData?: AuthApi;
  loading: boolean;
  email: string;
  password: string;
  errors: string[];
  inputValues: {
    email?: string;
  };
}

class LoginPage extends React.Component<{}, LoginPageState> {
  constructor(props: {}) {
    super(props)
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      loading: false,
      email: '',
      password: '',
      errors: [],
      inputValues: {},
    };
  }

  handleSubmit(event: FormEvent): void {
    event.preventDefault()
    const { email, password } = this.state;

    this.setState({ loading: true, inputValues: { email } });

    postLogin(email, password).then((res) => {
      this.setState({
        userData: res,
        errors: [],
        loading: false,
      })
      logInUser(res.username);
    })
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const { target } = event;
    const { name, value } = target;
    switch(name) {
      case 'email':
        this.setState({email: value})
        break;
      case 'password':
        this.setState({password: value})
        break;
    }

  }

  render = (): ReactNode => {
    const { loading, userData, inputValues } = this.state;
    const { email } = inputValues;
    const { username, errors = [] } = userData || {};
    if (username && errors.length < 1) {
      return <Redirect to={'/'} />
    }
    if (loading) {
      return <div className={'spinner'}></div>;
    } else {
      return (
        <>
          <h1>Log In</h1>
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId={'email'}>
              <Form.Label>Email address</Form.Label>
              <Form.Control required defaultValue={email} type={'email'} placeholder={'Enter email'} name={'email'} onChange={this.handleChange} />
              <Form.Text className={'text-muted'}>
                We&apos;ll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId={'password'} >
            <Form.Label>Password</Form.Label>
              <Form.Control required type={'password'} placeholder={'Password'} name={'password'} onChange={this.handleChange} />
            </Form.Group>
            <Button variant={'primary'} type={'submit'}>
              Submit
            </Button>
          </Form>
          {errors.length >0 && errors.map(
            (error, i) => {return <Alert key={i} className={'validation-error'} variant={'danger'}>{error}</Alert>}
          )}
        </>
      )
    }

  }
}

export default LoginPage;
