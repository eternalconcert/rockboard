import React, { FC } from 'react';
import { AuthApi } from '../../types';
import { fetchJson, usePromiseFactory } from '../../utils/helpers';
import { useParams } from 'react-router-dom';
import { baseApiUrl } from '../../app';
import { Alert } from 'react-bootstrap';

function fetchProfile(username?: string): Promise<AuthApi | null> {
  return fetchJson<AuthApi>(`${baseApiUrl}/profile/${username}`)
    .then(data => {
      return (
        {
          username: data.username,
          email: data.email,
          roles: data.roles,
          errors: [],
        }
      )
    }
  ).catch(error => {
    return (
      {
        username: username || '',
        email: '',
        roles: [''],
        errors: [error],
      }
    )
  })
}

const UserProfile: FC<{}> = () => {
  const { username } = useParams<{username: string}>();
  const profile = usePromiseFactory(fetchProfile, username);
  const { errors } = profile || {};
  if (errors && errors.length > 0) {
    return (
      <div>
        {profile?.errors.map((error) => {
          return <Alert key={error.statusCode} variant={'danger'}>{error.statusCode}: {error.message}</Alert>
        })}
      </div>
    )
  }
  else if (profile) {
      return <>Profile of {profile.username}</>
    }
    return <div className={'spinner'}></div>;
}

export default UserProfile;