import React, { FC } from 'react';
import { useParams, Link, Redirect } from 'react-router-dom';
import { usePromiseFactory, fetchJson } from '../../utils/helpers';
import { Topic, ForumWrapper } from '../../types';
import { Container, Card, Badge, Col, Row } from 'react-bootstrap';
import SectionHeader from '../section-header';
import { baseApiUrl } from '../../app';


interface TopicsApi {
  forumName: string;
  topics: Topic[];
}

function fetchTopics(forumId?: string): Promise<ForumWrapper> {
  return fetchJson<TopicsApi>(`${baseApiUrl}/topics/${forumId}`)
    .then(data => {
      return (
        {
          forumName: data.forumName,
          topics: data.topics,
          error: false,
        }
      )
    }
  ).catch(_ => {return {forumName: '', topics: [], error: true}})
}

const ForumOverview: FC<{}> = () => {
    const { forumId } = useParams<{forumId: string}>();

    const forumWrapper = usePromiseFactory(fetchTopics, forumId);
    if (forumWrapper) {
      const { topics, forumName, error } = forumWrapper;
      if (error) {
        return <Redirect to={'/error/500'} />;
      }
      return (
        <Container fluid={'md'}>
          <Row>
            <Col><SectionHeader name={forumName} /></Col>
          </Row>
          <Row>
              { topics && topics.map(topic => {
              const { id, name, latestPostBy, latestPostCreatedAt } = topic;
              const unread = true;
              return (
                <Col key={id} md={4}>
                  <Card>
                    <Card.Body>
                      <Card.Title>
                        <Link className={'card-title'} to={`/topics/${id}`}>{name}</Link>
                        { unread && <Badge className={'unread-badge'} variant={'dark'}>New</Badge> }
                      </Card.Title>
                      <Card.Text>{latestPostCreatedAt}<br /> by {latestPostBy}</Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              )
              }) }
          </Row>
        </Container>
      )
    }
    return <div className={'spinner'}></div>;
}



export default ForumOverview;
