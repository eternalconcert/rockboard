import React, { FC } from 'react';
import { Link } from 'react-router-dom';

const PostingToolbox: FC<{}> = (): JSX.Element => {
  return (
    <>
      <div className={'posting-toolbox'}>
        <Link to={'/'}>Edit</Link> <Link to={'/'}>Quote</Link> <Link to={'/'}>Report</Link>
      </div>
      <br/>
    </>
  )
}

export default PostingToolbox;