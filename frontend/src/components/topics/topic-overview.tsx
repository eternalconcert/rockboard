import React, { FC, useState, useEffect, useRef } from 'react';
import { Posting, TopicWrapper } from '../../types';
import { Container, Row, Col, Card, Alert, Button } from 'react-bootstrap';
import { fetchJson, usePromiseFactory, postJson } from '../../utils/helpers';
import { useParams, Link } from 'react-router-dom';
import ReplyForm from '../forms/reply-form';
import SectionHeader from '../section-header';
import parser from '../bbcode-tags/parser';
import { baseApiUrl } from '../../app';
import store from '../../flux/store';
import PostingToolbox from './posting-tool-box';

const CHUNK_SIZE = 5;

function fetchPostings(topicId?: string, offset?: number, limit?: number): Promise<TopicWrapper> {
  return fetchJson<TopicWrapper>(`${baseApiUrl}/postings/${topicId}?offset=${offset || '0'}&limit=${limit || `${CHUNK_SIZE}`}`)
    .then(data => {
      return (
        {
          topicName: data.topicName,
          postings: data.postings,
          totalLen: data.totalLen,
        }
      )
    }
  )
}


const TopicOverview: FC<{}> = (): JSX.Element => {
  const { topicId } = useParams<{topicId: string}>();
  const topicWrapper = usePromiseFactory(fetchPostings, topicId);
  const [postings, setPostings] = useState<Posting[]>([]);
  const [postLoading, setPostLoading] = useState(false);
    const username: string = store.getUserName();

  const offset = useRef(CHUNK_SIZE);
  const hasMore = useRef(true);

  useEffect(() => {
    const overscroll = (): void => {
      if ((window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) && hasMore.current) {
        setPostLoading(true);
        fetchPostings(topicId, offset.current, CHUNK_SIZE).then(r => {
          const newPostings = r.postings;
          setPostings(postings => [...postings, ...newPostings]);
          setPostLoading(false);
        })
        offset.current += CHUNK_SIZE;
      }
    };
    window.addEventListener('scroll', overscroll, false);

    return window.removeEventListener('scroll', overscroll, true);
  }, [topicId]);


  useEffect(() => {
    setPostings(topicWrapper?.postings || []);
  }, [topicWrapper]);

  if (topicWrapper) {
    const { topicName, totalLen } = topicWrapper;
    hasMore.current = (postings.length < totalLen);

    return (
      <Container fluid={'md'}>
        <Row>
          <Col><SectionHeader name={topicName} />
          {(username) ?
            <ReplyForm topicName={`Re: ${topicName}`} handleSubmit={(title: string, content: string): void => {
              postJson<Posting>(`${baseApiUrl}/postings/${topicId}`, {title, content}).then((newPosting) => {
                setPostings(postings => [newPosting, ...postings]);
              });
            }}
            /> : <Alert variant={'warning'}>You must be logged in to reply</Alert>
          }
          { postings && postings.map(posting => {
            const { id, title, content, createdBy, createdAt } = posting;
            return (
              <Card key={id}>
                <Card.Body>
                  <Card.Title>
                    {title || `Re: ${topicName}`}, by {username ? <Link to={`/user/profile/${createdBy}`}>{createdBy}</Link> : ` ${createdBy}`}
                    <span className={'card-date'}>{createdAt}</span>
                  </Card.Title>
                  <Card.Text className={'new-lines'}>{parser.toReact(content)}</Card.Text>
                  <PostingToolbox />
                </Card.Body>
              </Card>
            )})}
            { postLoading && <div className={'spinner bottom-spinner'}></div> }
            { !hasMore.current && <div className={'end-of-stream'}><Button onClick={(): void => window.scrollTo(0, 0)}>Back to top</Button></div> }
          </Col>
        </Row>
      </Container>
    )
  }
  return <div className={'spinner'}></div>;
}

export default TopicOverview;
