import React from 'react';


type PropsType = {
    name: string;
}


const SectionHeader = (props: PropsType): JSX.Element => {
    const { name } = props;
    return <div className={'category-header'}>{ name }</div>
};


export default SectionHeader;
