import React, { FC, useState, ChangeEvent } from 'react';
import { Form, Button } from "react-bootstrap"

type Props = {
  handleSubmit: Function;
  topicName: string;
}

const ReplyForm: FC<Props> = (props: Props): JSX.Element => {

  const [title, setTitle] = useState<string>('');
  const [content, setContent] = useState<string>('');
  const { handleSubmit, topicName } = props;

  const onSubmit = (event: React.FormEvent): void => {
    event.preventDefault();
    handleSubmit(title, content);
    setTitle('');
    setContent('');
  }

  return (
    <Form className={'reply-form'} onSubmit={onSubmit}>
      <Form.Group>
        <Form.Label>Title</Form.Label>
        <Form.Control type={'text'} value={topicName} onChange={(event: ChangeEvent<HTMLInputElement>): void => {setTitle(event.target.value)}} />
      </Form.Group>
      <Form.Group controlId={'reply-area'} >
        <Form.Label>Reply</Form.Label>
        <Form.Control as={'textarea'} value={content} onChange={(event: ChangeEvent<HTMLInputElement>): void => {setContent(event.target.value)}} rows={8} />
      </Form.Group>
      <Button type={'submit'} variant={'primary'}>Submit</Button>
    </Form>
  )
};

export default ReplyForm;