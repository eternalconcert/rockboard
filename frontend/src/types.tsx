import { HttpError } from "./exceptions";

export type Forum = {
  id: number;
  name: string;
  description: string;
  unread: boolean;
};

export type Category = {
  id: number;
  name: string;
  forums: Forum[];
};

export type Posting = {
  id: number;
  title: string;
  topic: number;
  content: string;
  createdAt: string;
  createdBy: number|string;
}

export type Topic = {
  id: number;
  name: string;
  latestPostBy: string;
  latestPostCreatedAt: string;
  postings: Posting[];
};

export type ForumWrapper = {
  forumName: string;
  topics: Topic[];
  error: boolean;
}

export type TopicWrapper = {
  topicName: string;
  postings: Posting[];
  totalLen: number;
}

export interface AuthApi {
  username: string;
  email: string;
  roles: string[];
  errors: HttpError[];
}

export type ActionType = {
  type: string;
  value: string;
}

export interface Dictionary<T> {
  [Key: string]: T;
}
