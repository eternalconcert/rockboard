import { EventEmitter } from 'events';
import typedDispatcher from './dispatcher';
import { ACTIONS } from './actions';
import { ActionType } from '../types';


class Store extends EventEmitter {
  private username: string;

  constructor() {
    super();
    this.username = '';
  }

  handleActions(action: ActionType): void {
    switch (action.type) {
      case ACTIONS.LOG_IN_USER: {
        this.username = action.value;
        this.emit('userLoggedIn');
        break;
      }
      case ACTIONS.LOG_OUT_USER: {
        this.username = '';
        this.emit('userLoggedOut');
        break;
      }
      default: {
        break;
      }
    }
  }

  getUserName(): string {
    return this.username;
  }
}

const store = new Store();
typedDispatcher.register(store.handleActions.bind(store));

export default store;