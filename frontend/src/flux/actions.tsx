import { Cookies } from 'react-cookie';
import typedDispatcher from './dispatcher';

const cookies = new Cookies();

export const ACTIONS = {
  LOG_IN_USER: 'userActions.logInUser',
  LOG_OUT_USER: 'userActions.logOutUser',
};

export function logInUser(username: string): void {
  if (username) {
    cookies.set('username', username, {path: '/'})
    typedDispatcher.dispatch({
      type: ACTIONS.LOG_IN_USER,
      value: username,
    })

  }
}

export function logOutUser(): void {
  cookies.remove('username', {path: '/'});
  typedDispatcher.dispatch({
    type: ACTIONS.LOG_OUT_USER,
    value: '',
  })
}
