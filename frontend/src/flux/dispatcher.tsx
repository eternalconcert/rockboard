import { Dispatcher } from 'flux';
import { ActionType } from '../types';


const typedDispatcher = new Dispatcher<ActionType>();

export default typedDispatcher;