import React, { FC, useEffect, useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import './styles/custom-bootstrap.css';

import Header from './components/header';
import Footer from './components/footer';
import Categories from './components/categories/categories';
import ForumOverview from './components/forums/forum-overview';
import LoginPage from './components/authentication/loginpage';
import LogoutPage from './components/authentication/logoutpage';
import UserProfile from './components/authentication/user-profile';
import { logInUser } from './flux/actions';
import { Cookies } from 'react-cookie';
import store from './flux/store';
import TopicOverview from './components/topics/topic-overview';
import typedDispatcher from './flux/dispatcher';
import AuthenticatedRoute from './utils/authenticated-route';
import ErrorPage from './utils/error-page';


const cookies = new Cookies();
export const baseApiUrl: string = process.env.REACT_APP_APIURL || '';


const App: FC = (): JSX.Element => {
  const [username, setUsername] = useState<string>('');

  const update = (): void => {
    setUsername(store.getUserName());
  }

  const cookyUsername = cookies.get('username') || '';

  useEffect(() => {
    store.on('userLoggedIn', update);
    store.on('userLoggedOut', update);
    if (username !== cookyUsername) {
      if (!typedDispatcher.isDispatching()) {
        logInUser(cookyUsername);
      }
    }
  })
  const boardName = process.env.REACT_APP_BOARDNAME || 'RockBoard';
  document.title = boardName;
  return (
    <Router>
      <div className={'content'}>
        <Header boardName={boardName} username={username} />
        <main role="main" className="container">
          <Switch>
            <Route exact path="/"><Categories /></Route>
            <Route path="/forums/:forumId"><ForumOverview /></Route>
            <Route path="/topics/:topicId"><TopicOverview /></Route>
            <Route path="/user/login"><LoginPage /></Route>
            <Route path="/user/logout"><LogoutPage /></Route>
            <AuthenticatedRoute authenticated={username !== ''} path="/user/profile/:username">
              <UserProfile />
            </AuthenticatedRoute>
            <Route path="/error/:statusCode"><ErrorPage /></Route>
            <Route path="*"><Redirect to={'/error/404'} /></Route>
          </Switch>
        </main>
        <Footer />
      </div>
    </Router>
  );

}

export default App;
