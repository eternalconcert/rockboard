from datetime import datetime
from time import sleep

from flask import abort, request, session
from flask_restful import Resource, reqparse

from decorators import login_required, sliced
from models import Category, User, Forum, Topic, Post


class InvalidCredentianls(Exception):
    pass


TEST_LATENCY=0.2


class Postings(Resource):

    @sliced
    def get(self, offset, limit, topic_id):
        sleep(TEST_LATENCY)
        try:
            topic = Topic.objects.get(id=topic_id)
        except IndexError as e:
            abort(404)
        postings_query = topic.postings.all().order_by('-created_at')
        postings = postings_query[offset:offset+limit]
        return {
            'topicName': topic.name,
            'totalLen': topic.postings.count(),
            'postings': [
                {
                    'id': posting.id,
                    'topicId': topic.id,
                    'createdBy': posting.created_by().username,
                    'createdAt': str(posting.created_at),
                    'title': posting.title,
                    'content': posting.content
                } for posting in postings
            ]
        }

    @login_required
    def post(self, topic_id):
        post = Post.objects.create(
            title=request.json['title'],
            content=request.json['content'],
            topic=Topic.objects.get(id=topic_id),
            created_by=session['user_id'],
            created_at=datetime.now()
        )
        return {
            'id': post.id,
            'title': post.title,
            'content': post.content,
            'topic': post.topic().id,
            'createdBy': post.created_by().username,
            'createdAt': str(post.created_at),
        }


class Topics(Resource):
    def get(self, forum_id):
        sleep(TEST_LATENCY)
        try:
            forum = Forum.objects.get(id=forum_id)
        except IndexError as e:
            abort(404)
        topics = Topic.get_ordered_by_latest_post_for_forum(forum.id)
        return {
            'forum_name': forum.name,
            'topics': [
                {'id': topic.id,
                 'name': topic.name,
                 'latestPostBy': User.objects.filter(id=topic.latest_post_created_by).first().username if User.objects.filter(id=topic.latest_post_created_by).exists() else "GNARF",
                 'latestPostCreatedAt': str(topic.latest_post_created_at) if topic.latest_post_created_at else "GNARF"} for topic in topics]}



class Categories(Resource):
    def get(self):
        sleep(TEST_LATENCY)
        return {
            'categories': [
                {
                    'id': category.id, 'name': category.name, 'forums': [
                        {
                            'id': forum.id,
                            'name': forum.name,
                            'description': forum.description,
                            'unread': bool(forum.unread)
                        } for forum in category.forums.all()
                    ]
                } for category in Category.objects.all().order_by('sort_order')
            ]
        }


class Login(Resource):
    def post(self):
        sleep(TEST_LATENCY)
        try:
            email = request.json['email']
            password = request.json['password']
            user = User.objects.get(email=email)
            if not user.validate_password(password):
                raise InvalidCredentianls()
            session['username'] = user.username
            session['user_id'] = user.id
            return {'email': user.email, 'username': user.username, 'roles': ['admin', 'user'], 'errors': []}
        except:
            return {'email': None, 'username': None, 'roles': [], 'errors': ['User could not be logged in']}


class Logout(Resource):
    def get(self):
        sleep(TEST_LATENCY)
        if session.get('username'):
            session.pop('username')
        return {'success': True}


class Profile(Resource):
    @login_required
    def get(self, username):
        sleep(TEST_LATENCY)
        return {'username': username}
