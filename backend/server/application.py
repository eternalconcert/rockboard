import os

from flask import Flask
from flask_restful import Api
from flask_cors import CORS

from resources import Categories, Login, Logout, Profile, Postings, Topics


app = Flask(__name__)
app.secret_key = os.environ['SECRET_KEY']
CORS(app, supports_credentials=True)
api = Api(app)


api.add_resource(Categories, '/categories/')
api.add_resource(Topics, '/topics/<int:forum_id>')
api.add_resource(Postings, '/postings/<int:topic_id>')
api.add_resource(Login, '/login')
api.add_resource(Logout, '/logout')
api.add_resource(Profile, '/profile/<string:username>')
