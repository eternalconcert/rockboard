import os
import sys

from models import Migration

migration = Migration()

commands = {
    'run': migration.run_migrations,
    'create': migration.create_migrations
}


migrations_dir = 'migrations'

if not os.path.exists(migrations_dir):
    os.makedirs(migrations_dir)


commands.get(sys.argv[1])()
