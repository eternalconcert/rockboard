import json
import re
import sys


from models import Category, Forum, Post, User, Topic


file_path = sys.argv[1]


def repl_smileys(m):
    smiley = m.group(2)
    return f"[smiley={smiley}]"

content = None
with open(file_path, 'r') as f:
    content = f.read()
    content = content.replace('<t>', '')
    content = content.replace('</t>', '')
    content = content.replace('<s>', '')
    content = content.replace('</s>', '')
    content = content.replace('<r>', '')
    content = content.replace('</r>', '')
    content = content.replace('<e>', '')
    content = content.replace('</e>', '')
    content = content.replace('&gt;', '>')
    content = content.replace('&lt;', '<')
    content = content.replace('&amp;', '&')
    content = content.replace('<br/>', '\\n')

    content = re.sub(r'<QUOTE author=\\\".*?\\\">', "", content)
    content = content.replace('</QUOTE>', '')

    content = re.sub(r'<COLOR color=\\\".*?\\\">', "", content)
    content = content.replace('</COLOR>', '')

    content = re.sub(r"(<E>:)(.*?)(:</E>)", repl_smileys, content)


class UserCache:


    def __init__(self):
        self.count_cached = 0
        self.count_fetched = 0
        self.cache = {}

    def get(self, username):
        try:
            self.count_cached += 1
            return self.cache[username]
        except:
            self.count_fetched += 1
            user = User.objects.get(username=username)
            self.cache[username] = user
            return user

user_cache = UserCache()


user_bulk = []
user_mapping = {}
dump = json.loads(content)
users = dump['users']
print(f"Creating {len(users)} users")
for user in users:
    user_bulk.append(User(username=user['username'], email=user['user_email'], password_hash=user['user_password']))
    user_mapping[user['user_id']] = user['username']
User.objects.bulk_create(user_bulk)


forums = dump['forums']
forum_mapping = {}
category_mapping = {}
print(f"Creating categories")
for forum in forums:
    if forum['parent_id'] == 0:
        category = Category.objects.create(name=forum['forum_name'], sort_order=forum['forum_id'])
        category_mapping[forum['forum_id']] = category
        forum_mapping[forum['forum_id']] = category

print(f"Creating {len(forums)} forums")
for sub_forum in forums:
    if sub_forum['parent_id'] != 0:
        created = Forum.objects.create(category=category_mapping[sub_forum['parent_id']], name=sub_forum['forum_name'], description=sub_forum['forum_desc'])
        forum_mapping[sub_forum['forum_id']] = created

topics_bulk = []
topics_mapping = {}
topics = dump['topics']
print(f"Creating {len(topics)} topics")
topic_count = 1
for topic in topics:
    topics_bulk.append(Topic(id=topic_count, created_by=user_cache.get(user_mapping[topic['topic_poster']]).id, forum=forum_mapping[topic['forum_id']].id, name=topic['topic_title']))
    topics_mapping[topic['topic_id']] = topic_count
    topic_count += 1
Topic.objects.bulk_create(topics_bulk)


posts_bulk = []
posts = dump['posts']
posts_mapping = {}
print(f"Creating {len(posts)} posts")
post_count = 1
for post in posts:
    posts_bulk.append(Post(id=post_count, topic=topics_mapping[post['topic_id']], created_by=user_cache.get(user_mapping[post['poster_id']]).id, created_at=post['post_time'], title=post['post_subject'], content=post['post_text']))
    posts_mapping[post['post_id']] = post_count
    post_count += 1
Post.objects.bulk_create(posts_bulk)


print('cached: ', user_cache.count_cached)
print('fetched: ', user_cache.count_fetched)