from functools import wraps

from flask import abort, session
from flask_restful import reqparse

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if "username" in session:
            return func(*args, **kwargs)
        else:
            abort(401)
    return wrapper


def sliced(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        parser = reqparse.RequestParser()
        parser.add_argument('offset', default='0')
        parser.add_argument('limit', default='10')
        params = parser.parse_args()
        return func(*args, int(params.offset), int(params.limit), **kwargs)
    return wrapper