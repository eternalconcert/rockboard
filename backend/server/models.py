import importlib
import json
import os

import nopea
from nopea.migrations import Migration
from passlib.hash import bcrypt, argon2

Migration.migration_dir = os.path.join(os.getcwd(), 'migrations')
DbObject = nopea.DbObject
adaptor = importlib.import_module(os.environ.get('DB_MODULE', 'db')).adaptor
DbObject.adaptor = adaptor


class User(DbObject):

    def __repr__(self):
        return f"<User: {self.username}>"

    username = nopea.CharField(max_length=255)
    email = nopea.CharField(max_length=255)
    password_hash = nopea.CharField(max_length=255)
    is_admin = nopea.BooleanField(default=False)
    is_mod = nopea.BooleanField(default=False)

    def validate_password(self, password):
        if self.password_hash.startswith("$argon2"):
            return argon2.verify(password, self.password_hash)
        return bcrypt.verify(password, self.password_hash)

    # bcrypt.using(rounds=13).hash("password")
    #    => $2b$13$ewbr1jufaXfyp92Bjb/zE.OGQ5nttjbcN9OJEZOqtag5WHvdUOyDO


class Category(DbObject):
    name = nopea.CharField(max_length=255)
    sort_order = nopea.IntegerField()

    def __repr__(self):
        return f"<Category: {self.name}>"


class Forum(DbObject):
    name = nopea.CharField(max_length=255)
    description = nopea.TextField()
    unread = nopea.BooleanField(default=True)
    category = nopea.ForeignKey(Category, reverse_name='forums', lazy=True)

    def __repr__(self):
        return f"<Forum: {self.name}>"


class Topic(DbObject):
    def __repr__(self):
        return f"<Topic: {self.name}>"

    created_by = nopea.ForeignKey(User, lazy=True)
    name = nopea.CharField(max_length=255)
    forum = nopea.ForeignKey(Forum, reverse_name='topics', lazy=True)

    def __init__(self, *args, **kwargs):
        self.latest_post_title = kwargs.get('latest_post_title')
        self.latest_post_created_by = kwargs.get('latest_post_created_by')
        self.latest_post_created_at = kwargs.get('latest_post_created_at')
        super().__init__(*args, **kwargs)

    @classmethod
    def get_ordered_by_latest_post_for_forum(cls, forum_id):
        result = Topic.raw(
            '''SELECT topic.id,
                   topic.created_by,
                   topic.name,
                   topic.forum,
                   POST.title,
                   POST.created_by,
                   POST.created_at
               FROM   topic
                   left join(SELECT *
                               FROM   (SELECT DISTINCT ON (post.topic) post.topic,
                                                                       post.created_at,
                                                                       post.title,
                                                                       post.created_by
                                       FROM   post
                                       ORDER  BY post.topic,
                                               post.created_at DESC) s
                               ORDER  BY created_at DESC) POST
                           ON POST.topic = topic.id
               WHERE  topic.forum = %s''', forum_id
        )
        return [
            cls(
                id=item[0],
                created_by=item[1],
                name=item[2],
                forum=item[3],
                latest_post_title=item[4],
                latest_post_created_by=item[5],
                latest_post_created_at=item[6])
            for item in result
        ]


class Post(DbObject):
    topic = nopea.ForeignKey(Topic, reverse_name='postings', lazy=True)
    created_by = nopea.ForeignKey(User, lazy=True)
    created_at = nopea.DateTimeField()
    title = nopea.CharField(max_length=255)
    content = nopea.TextField()
