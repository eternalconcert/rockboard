import os
from nopea.adaptors.postgres import PostgreSQLAdaptor


adaptor = PostgreSQLAdaptor({
    'user': os.environ['PG_USER'],
    'password': os.environ['PG_PASSWORD'],
    'host': os.environ['PG_HOST'],
    'port': os.environ['PG_PORT'],
    'database': os.environ['PG_DATABASE']
})
